# teste-front-end

## Setup do projeto
```
yarn install
```

### Testar o projeto local
```
yarn serve
```

### Compilar o projeto para produção
```
yarn build
```